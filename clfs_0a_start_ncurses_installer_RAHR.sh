#/bin/bash
#Install script needs ncurses and dialog package

function checkRequirements() {
echo
echo "Does your system fullfil the requirements to build CLFS?: [Y/N]"
echo
while read -n1 -r -p "" && [[ $REPLY != q ]]; do
  case $REPLY in
    Y) break 1;;
    N) echo "$EXIT"
       echo "Fix it!"
       exit 1;;
    *) echo " Try again. Type y or n";;
  esac
done
echo
}

function isDialogInstalled() {
echo
echo "Are ncurses and dialog installed?: [Y/N]"
echo
while read -n1 -r -p "" && [[ $REPLY != q ]]; do
  case $REPLY in
    Y) break 1;;
    N) echo "$EXIT"
       echo "Fix it!"
       exit 1;;
    *) echo " Try again. Type y or n";;
  esac
done
echo
}

function selectPartition() {

devicetitle=$1

if [[ $2 == "" && $3 == "" ]]; then
  devices=$(lsblk | tail -n $(expr $(lsblk | wc -l) - 1) | awk {'print $1'} | grep "sd[a-z]\|nvme[0-9]n[0-9]p[0-9]" | sed 's/^\(|-\)\|^\(`-\)//g' | sed '/^\s*$/d' | sed 's/^/\/dev\//g' | sed 's/\s/\n/g')
  devicecount=$(lsblk | tail -n $(expr $(lsblk | wc -l) - 1) | awk {'print $1'} | grep "sd[a-z]\|nvme[0-9]n[0-9]p[0-9]" | sed 's/^\(|-\)\|^\(`-\)//g' | sed '/^\s*$/d' | sed 's/^/\/dev\//g' | sed 's/\s/\n/g' | wc -l)
elif [[ $2 != ""  && $3 == "" ]]; then
  modarg2=$(echo "$2" | sed 's/\//\\\//g')
  devices=$(echo "$devices" | sed "s/${modarg2}//g" |  sed '/^\s*$/d' | sed 's/\s/\n/g')
  devicecount=$(expr $devicecount - 1)
elif [[ $3 != "" && $2 != "" ]]; then
  modarg3=$(echo "$3" | sed 's/\//\\\//g')
  devices=$(echo "$devices" | sed "s/${modarg3}//g" | sed '/^\s*$/d' | sed 's/\s/\n/g')
  devicecount=$(expr $devicecount - 1)
fi

dialogcmd="dialog --backtitle "
dialogcmd+=" \"Select\" "
dialogcmd+=" --radiolist "
dialogcmd+=" \"${devicetitle}\" "
dialogcmd+=" 15 45 "
dialogcmd+=${devicecount}
dialogcmd+=" "

devicearray=()
j=0

for (( i=1;i<=${devicecount};i++ ))
do
  j=$(expr ${i} - 1)
  devicearray[${j}]=$(echo ${devices} | cut -d' ' -f${i})
  dialogcmd+=" $i ${devicearray[$j]} off "
done

exec 3>&1;

result=$(${dialogcmd} 2>&1 1>&3)
exitcode=$?
exec 3>&-

chosendrive=$(echo ${devicearray[$(expr ${result} - 1)]})
}

function selectFs() {
  fsarray=(ext4 xfs btrfs)

  exec 3>&1;
  bulletpoint=$(dialog --backtitle "Choose the filesystem" --radiolist "for your home partition" 10 40 3 1 ${fsarray[0]} on 2 ${fsarray[1]} off 3 ${fsarray[2]} off 2>&1 1>&3);
  exitcode=$?;
  exec 3>&-;

  chosenfs=$(echo ${fsarray[$(expr ${bulletpoint} - 1)]})
}

function yesNo() {  
  dialog --title "Format $1?" --yesno "Do you want to format your $1 partition?" 10 25
  
  if [[ $? == 0 ]]; then
    choice=yes
  elif [[ $? == 1 ]]; then
    choice=no
  fi
}

function catchUserInput() {
  question=$1
  dialog --inputbox ${question} 8 40 2>answer
  input=$(cat answer)
  echo > answer
}

printf "\033c"

#Simple script to list version numbers of critical development tools
#This script is from the LFS/CLFS developers. It is not my work. Thanks to them :)

basedirectory="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

sed -i '5481,5485 s/({/(\\{/' /usr/share/texinfo/Texinfo/Parser.pm

echo
bash ${basedirectory}/meta/version-check.sh 2>errors.log &&
[ -s errors.log ] && echo -e "\nThe following packages could not be found:\n$(cat errors.log)"
echo

checkRequirements
isDialogInstalled

if [[ ${EUID} != 0 ]]; then
  exit
fi

#Clean old CLFS system install parameters
echo > clfs-system.config

selectPartition ESP
clfsespdev=${chosendrive}

selectPartition ROOT ${clfsespdev}
clfsrootdev=${chosendrive}

selectPartition HOME ${clfsespdev} ${clfsrootdev}
clfshomedev=${chosendrive}

yesNo home
clfsformathomedev=${choice}
echo "formathome=${clfsformathomedev}" >> clfs-system.config

yesNo esp
clfsformatespdev=${choice}
echo "formatesp=${clfsformatespdev}" >> clfs-system.config

selectFs
clfsfilesystem=${chosenfs}
echo "fs=${clfsfilesystem}" >> clfs-system.config

if [[ ${clfsfilesystem} != ext4 ]]; then
  echo "Filesystems other than ext4 are not supported for now! Exiting..."
  exit;
fi

constructarg="Kernel"

catchUserInput ${constructarg}

kernelver=${input}

kernelmajor=$(echo ${kernelver} | cut -d'.' -f1)
kernelminor=$(echo ${kernelver} | cut -d'.' -f2 | sed 's/-rc[0-9]//g')
kernelpatch=$(echo ${kernelver} | cut -d'.' -f3)
kernelrcver=""

if [[ -z "${kernelver##*-rc*}" ]]; then
  kernelrcver=$(echo ${kernelver} | cut -d'-' -f2)
fi

if [[  ${kernelver} != git ]]; then
  if [[ ${kernelmajor} < 4 ]]; then
    echo "You need to chose a more up-to-date kernel version! Exiting..."
    exit;
  fi

  if [[ ${kernelmajor} < 4 && {kernelminor} < 14 ]]; then
    echo "You need to chose a more up-to-date kernel version! Exiting..."
    exit;
  fi

  if [[ ${kernelrcver} != "" && ${kernelpatch} != "" ]]; then
    echo "You chose a RC version of a kernel. In this case only type in minor and major version."
    echo "RIGHT: 4.16-rc4"
    echo "WRONG: 4.16.0-rc4"
    echo "Exiting..."
    exit;
  fi
fi

pickhostname="Hostname"
catchUserInput ${pickhostname}
clfshostname=${input}

pickusername="Username"
catchUserInput ${pickusername}
clfsusername=${input}

CLFS=/mnt/clfs
CLFSUSER=clfs
CLFSHOME=${CLFS}/home
CLFSHOSTNAME=${clfshostname}
CLFSUSERNAME=${clfsusername}
CLFSROOTDEV=${clfsrootdev}
CLFSESPDEV=${clfsespdev}
CLFSHOMEDEV=$clfshomedev
CLFSSOURCES=${CLFS}/sources
CLFSTOOLS=${CLFS}/tools
CLFSFILESYSTEM=${clfsfilesystem}
CLFSCROSSTOOLS=${CLFS}/cross-tools

echo "kernel=${kernelver}" >> clfs-system.config
echo "hostname=${CLFSHOSTNAME}" >> clfs-system.config
echo "username=${CLFSUSERNAME}" >> clfs-system.config
echo "clfsrootdev=${CLFSROOTDEV}" >> clfs-system.config
echo "clfshomedev=${CLFSHOMEDEV}" >> clfs-system.config

printf "\033c"

mkfs.${CLFSFILESYSTEM} -q ${CLFSROOTDEV}
echo

if [[ ${clfsformathomedev} = yes ]]; then
  mkfs.${CLFSFILESYSTEM} -q ${CLFSHOMEDEV}
fi

echo

if [[ ${clfsformatespdev} = yes ]]; then
  mkfs.vfat -v -F32 ${CLFSESPDEV}
fi

echo "espdev=${CLFSESPDEV}" >> clfs-system.config

mkdir -pv $CLFS
mount -v ${CLFSROOTDEV} ${CLFS}
mkdir -pv $CLFSHOME
mount -v ${CLFSHOMEDEV} ${CLFSHOME}

mkdir -v ${CLFSSOURCES}
chmod -v a+wt ${CLFSSOURCES}

echo

#rm -rf gnulib
#git clone git://git.savannah.gnu.org/gnulib.git
#cp -rv gnulib sources/
cp -rv ${basedirectory}/sources/* ${CLFSSOURCES}
#rm -rf gnulib

#Download kernel and toolchain
wget http://ftp.gnu.org/gnu/binutils/binutils-2.31.1.tar.bz2 -P ${CLFSSOURCES}
wget ftp://gcc.gnu.org/pub/gcc/releases/gcc-8.2.0/gcc-8.2.0.tar.xz -P ${CLFSSOURCES}
if [[ $kernelpatch != "" && $kernelver != git ]]; then
    wget https://cdn.kernel.org/pub/linux/kernel/v$kernelmajor.x/linux-$kernelmajor.$kernelminor.$kernelpatch.tar.xz -P ${CLFSSOURCES}
elif [[ $kernelpatch = "" && $kernelrcver = "" && $kernelver != git ]]; then
    wget https://cdn.kernel.org/pub/linux/kernel/v$kernelmajor.x/linux-$kernelmajor.$kernelminor.tar.xz -P ${CLFSSOURCES}
elif [[ $kernelpatch = "" && $kernelrcver != "" && $kernelver != git ]]; then
    wget https://git.kernel.org/torvalds/t/linux-$kernelmajor.$kernelminor-$kernelrcver.tar.gz -P ${CLFSSOURCES}
elif [[ $kernelver = git ]]; then
    git clone https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
fi

wget https://ftp.gnu.org/gnu/glibc/glibc-2.28.tar.xz -P ${CLFSSOURCES}

echo

echo "We also need to download Python 3.7 because the file exceed GitLabs's upload size limit!"

echo

wget https://www.python.org/ftp/python/3.7.1/Python-3.7.1.tar.xz -P ${CLFSSOURCES}

echo "Source packages have been copied to ${CLFS} and are now owned by clfs:clfs"

install -dv ${CLFSTOOLS}
install -dv ${CLFSCROSSTOOLS}
ln -sv ${CLFSCROSSTOOLS} /
ln -sv ${CLFSTOOLS} /

groupadd ${CLFSUSER}
useradd -s /bin/bash -g ${CLFSUSER} -d /home/${CLFSUSER} ${CLFSUSER}
mkdir -pv /home/${CLFSUSER}
chown -v ${CLFSUSER}:${CLFSUSER} /home/${CLFSUSER}
chown -v ${CLFSUSER}:${CLFSUSER} ${CLFSTOOLS}
chown -v ${CLFSUSER}:${CLFSUSER} ${CLFSCROSSTOOLS}
chown -R ${CLFSUSER}:${CLFSUSER} ${CLFSSOURCES}

echo "Sources are owned by clfs:clfs now"

cp -v ${basedirectory}/clfs_1_*.sh clfs_2_*.sh clfs_3_*.sh /home/${CLFSUSER}
cp -v ${basedirectory}/clfs_{5,6,7,8,9,10,11,12,13}*.sh ${CLFS}
mv -v ${basedirectory}/clfs-system.config ${CLFS}
cp -rv ${basedirectory}/meta ${CLFS}
cp -v ${basedirectory}/meta/ineedtopee.sh /home/${CLFSUSER}
cp -v ${basedirectory}/meta/functions_*_c2.sh /home/${CLFSUSER}
cp -v ${basedirectory}/meta/functions_*_c3.sh /home/${CLFSUSER}
chown -R ${CLFSUSER}:${CLFSUSER} /home/${CLFSUSER}

echo
echo "All install scripts have been copied to ${CLFS}"
echo "Check the screen output if everything looks fine"
echo "Executing script 0b to login as unprivilidged CLFS user..."
echo

source meta/clfs_0b_login_as_clfs_RAHR.sh
